<?php

use App\Http\Controllers\AuthController;
use App\Http\Livewire\AccountSettingProfile;
use App\Http\Livewire\AccountSettingYape;
use App\Http\Livewire\DashboardGeneral;
use App\Http\Livewire\Empresa;
use App\Http\Livewire\PageEmpresa;
use App\Http\Livewire\PageOferta;
use App\Http\Livewire\PageOfertaDetalle;
use App\Http\Livewire\PageOfertaDetallePostulacion;
use App\Http\Livewire\PagePostulacion;
use App\Http\Livewire\SecurityPermissions;
use App\Http\Livewire\SecurityRoles;
use App\Http\Livewire\TableCategories;
use App\Http\Livewire\Tableempresas;
use App\Http\Livewire\TableOfertas;
use App\Http\Livewire\TablePostulacion;
use App\Http\Livewire\TableProducts;
use App\Http\Livewire\TableUsers;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('/');

// RUTAS DE LOGIN/REGISTER/LOGOUT/UPDATE/DESTROY PERSONALIZADOS
Route::controller(AuthController::class)->group(function () {
    Route::post('/logout_user', 'logout_user')->name('logout_user');
    Route::post('/register_user', 'create_user')->name('register_user');
    Route::post('/iniciar_user', 'login_user')->name('iniciar_user');
});

// RUTAS DE VERIFICACIÓN PERSONALIZADOS
Route::get('/email/verify-notice', 'App\Http\Controllers\VerificacionController@notice')->name('verificacion.notice');
Route::get('/email/verify/{id}', 'App\Http\Controllers\VerificacionController@verify')->name('verificacion.verify');
Route::get('/email/resend', 'App\Http\Controllers\VerificacionController@resend')->name('verificacion.resend');

Route::middleware(['auth:sanctum', config('jetstream.auth_session'), 'verified'])->group(function () {
    Route::get('/sistema/pagina/dashboard-general', DashboardGeneral::class)->name('dashboard-general');
    Route::get('/sistema/pagina/dashboard-ventas', DashboardGeneral::class)->name('dashboard-ventas');

    Route::get('/sistema/pagina/configurar-cuenta-perfil', AccountSettingProfile::class)->name('configurar-cuenta-perfil');
    Route::get('/sistema/pagina/configurar-cuenta-yape', AccountSettingYape::class)->name('configurar-cuenta-yape');

    Route::get('/sistema/pagina/seguridad-roles', SecurityRoles::class)->name('seguridad-roles');
    Route::get('/sistema/pagina/seguridad-permisos', SecurityPermissions::class)->name('seguridad-permisos');
    Route::get('/sistema/pagina/favoritos', AccountSettingProfile::class)->name('favoritos');
    Route::get('/sistema/pagina/mensajes', AccountSettingProfile::class)->name('mensajes');

    Route::get('/sistema/pagina/tabla-usuarios', TableUsers::class)->name('tabla-usuarios');
    Route::get('/sistema/pagina/tabla-categorias', TableCategories::class)->name('tabla-categorias');
    Route::get('/sistema/pagina/tabla-productos', TableProducts::class)->name('tabla-productos');
    Route::get('/sistema/pagina/tabla-empresas', Tableempresas::class)->name('tabla-empresas');
    Route::get('/sistema/pagina/tabla-ofertas', TableOfertas::class)->name('tabla-ofertas');
    Route::get('/sistema/pagina/tabla-postulacion', TablePostulacion::class)->name('tabla-postulacion');

    Route::get('/sistema/pagina/ofertas-laborales', PageOferta::class)->name('ofertas-laborales');
    Route::get('/sistema/pagina/oferta-laboral-detalle/ofertaId/{ofertaId}', PageOfertaDetalle::class)->name('oferta-laboral-detalle');
    Route::get('/sistema/pagina/postulacion', PageOfertaDetallePostulacion::class)->name('oferta-laboral-detalle-postulacion');
    Route::get('/sistema/pagina/postulaciones', PagePostulacion::class)->name('postulaciones');
    Route::get('/sistema/pagina/empresa', PageEmpresa::class)->name('empresa');

    Route::get('/sistema/pagina/tabla-venta-clientes', AccountSettingProfile::class)->name('tabla-venta-clientes');
    Route::get('/sistema/pagina/tabla-venta-entregas', AccountSettingProfile::class)->name('tabla-venta-entregas');
    Route::get('/sistema/pagina/registro-de-ventas-listado-de-ventas', AccountSettingProfile::class)->name('registro-de-ventas');
    Route::get('/sistema/pagina/registro-de-ventas-pagos-yape', AccountSettingProfile::class)->name('registro-de-ventas-pagos-yape');
    Route::get('/sistema/pagina/registro-de-ventas-productos-vendidos', AccountSettingProfile::class)->name('registro-de-ventas-productos-vendidos');

    Route::get('/sistema/pagina/registro-de-compras-listado-de-compras', AccountSettingProfile::class)->name('registro-de-compras');
    Route::get('/sistema/pagina/registro-de-compras-pagos-yape', AccountSettingProfile::class)->name('registro-de-compras-pagos-yape');
    Route::get('/sistema/pagina/registro-de-compras-productos-comprados', AccountSettingProfile::class)->name('registro-de-compras-productos-comprados');
});

require_once __DIR__ . '/jetstream.php';
require_once __DIR__ . '/fortify.php';

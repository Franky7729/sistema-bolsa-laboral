<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Postulacion extends Model
{
    use HasFactory;

    protected $fillable = ['tipo', 'seleccionado', 'fecha_hora_postulacion' , 'oferta_laboral_id', 'user_id', 'ruta_pdf'];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function oferta_laboral()
    {
        return $this->belongsTo(Oferta_laboral::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }
}

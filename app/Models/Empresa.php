<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    use HasFactory;

    protected $fillable = ['razon_social', 'ruc', 'direccion', 'telefono', 'correo', 'creador', 'user_id'];

    //Relación 1 a * inversa
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Oferta_laboral extends Model
{
    use HasFactory;

    protected $fillable = ['titulo', 'descripcion', 'fecha_publicacion' , 'fecha_cierre' , 'remuneracion', 'ubicacion', 'tipo', 'cantidad', 'empresa_id'];

    public function empresa()
    {
        return $this->belongsTo(Empresa::class);
    }

    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    public function usuarios()
    {
        return $this->belongsToMany(User::class, 'oferta_usuario');
    }

    public function postulaciones()
    {
        return $this->hasMany(Postulacion::class);
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Empresa;
use App\Models\Oferta_laboral;
use App\Models\Postulacion;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class PageOferta extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $isOpen = false,
        $ruteCreate = false;
    public $ofertaState,
        $ofertaCategory,
        $amount = 10;
    public $search, $oferta, $image;
    public $ofertaSeleccionadaId;


    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'oferta.titulo' => 'required',
        'oferta.descripcion' => 'required',
        'oferta.fecha_publicacion' => 'required',
        'oferta.fecha_cierre' => 'required',
        'oferta.remuneracion' => 'required',
        'oferta.ubicacion' => 'required',
        'oferta.tipo' => 'required',
        'oferta.empresa_id' => 'required',
    ];


    public function seleccionarOferta($ofertaId)
    {
        $this->ofertaSeleccionadaId = $ofertaId;
    }

    public function render()
    {

        // Obtener el usuario autenticado
        $user = Auth::user();
        $empresa = Empresa::where('user_id', $user->id)->first();
        $empresaId = $empresa->id ?? null;

        // Asignar el ID de la empresa a la oferta
        $this->oferta['empresa_id'] = $empresaId;

        // Obtener todas las ofertas
        $ofertasQuery = Oferta_laboral::where('titulo', 'like', '%' . $this->search . '%')->latest('id');

        // Obtener la primera oferta disponible si no hay una seleccionada
        if (!$this->ofertaSeleccionadaId) {
            $ofertaSeleccionada = $ofertasQuery->first();
            $this->ofertaSeleccionadaId = $ofertaSeleccionada ? $ofertaSeleccionada->id : null;
        } else {
            // Obtener detalles de la oferta seleccionada
            $ofertaSeleccionada = Oferta_laboral::find($this->ofertaSeleccionadaId);
        }

        // Paginar las ofertas
        $ofertas = $ofertasQuery->paginate($this->amount);

        $ofertasConteo = Oferta_laboral::count();

        return view('pages.ofertas', compact('ofertas', 'ofertasConteo', 'ofertaSeleccionada'));
    }


    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('oferta', 'image');
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->oferta['id'])) {
            $oferta = Oferta_laboral::create($this->oferta);
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                $oferta->image()->create([
                    'url' => $image,
                ]);
            }
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $oferta = Oferta_laboral::find($this->oferta['id']);
            // $oferta->update($this->oferta);
            $oferta->update(Arr::except($this->oferta, 'image'));
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                if ($oferta->image) {
                    Storage::disk('public')->delete('galery', $oferta->image->url);
                    $oferta->image()->update([
                        'url' => $image,
                    ]);
                } else {
                    $oferta->image()->create([
                        'url' => $image,
                    ]);
                }
            }
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'oferta', 'image']);
        $this->emitTo('TableOfertas', 'render');
    }

    public function edit($oferta)
    {
        // Guardar los datos en sesión
        session(['ofertaId' => $oferta]);

        // Realiza la redirección a la página de detalles de la oferta
        return redirect()->route('oferta-laboral-detalle-postulacion');
    }

    public function delete($id)
    {
        Oferta_laboral::find($id)->delete();
    }
}

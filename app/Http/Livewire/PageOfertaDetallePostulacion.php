<?php

namespace App\Http\Livewire;

use App\Models\Oferta_laboral;
use App\Models\Postulacion;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

class PageOfertaDetallePostulacion extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $postulacion = [];
    public $usuarioLogueado;
    public $ofertasLaborales, $ofertaLaboralSeleccionadaId;
    public $document; // Añadida propiedad para el archivo PDF

    protected $rules = [
        'postulacion.user_id' => 'required',
        'postulacion.oferta_laboral_id' => 'required',
        'postulacion.fecha_hora_postulacion' => 'required',
        'postulacion.tipo' => 'required',
        'document' => 'nullable|file|mimes:pdf|max:2048', // Reglas de validación para el PDF
    ];

    public function render()
    {
        $ofertaId = Session::get('ofertaId');

        $this->ofertaLaboralSeleccionadaId = $ofertaId;
        $this->postulacion['oferta_laboral_id'] = $this->ofertaLaboralSeleccionadaId;

        $this->usuarioLogueado = Auth::user();

        $this->ofertasLaborales = Oferta_laboral::pluck('titulo', 'id')->all();

        $this->postulacion['user_id'] = $this->usuarioLogueado->id;

        session()->forget('active_menu');

        return view('pages.oferta-detalles-postulacion');
    }

    public function store()
    {
        $this->validate();

        // Procesar el archivo PDF si se ha proporcionado
        if ($this->document) {
            // Guardar el archivo PDF en el almacenamiento
            $pdfPath = $this->document->store('pdfs', 'public');

            // Asignar la ruta del PDF a la postulación
            $this->postulacion['ruta_pdf'] = $pdfPath;
        }

        // Guardar la postulación en la base de datos
        Postulacion::create($this->postulacion);

        // Luego de guardar, puedes emitir un evento o mensaje
        session()->flash('message', '¡Postulación realizada con éxito!');

        // Limpiar el formulario después de la postulación
        $this->reset(['postulacion', 'document']);
    }
}

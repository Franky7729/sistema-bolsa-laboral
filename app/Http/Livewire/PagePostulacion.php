<?php

namespace App\Http\Livewire;

use App\Models\Oferta_laboral;
use App\Models\Postulacion;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;

class PagePostulacion extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $isOpen = false,
        $ruteCreate = false;
    public $ofertaState,
        $ofertaCategory,
        $amount = 5;
    public $search, $oferta, $image;
    public $ofertaId;
    public $postulaciones;
    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'postulaciones.user_id' => 'required',
        'postulaciones.oferta_laboral_id' => 'required',
        'postulaciones.fecha_hora_postulacion' => 'required',
        'postulaciones.tipo' => 'required',
        'postulaciones.seleccionado' => 'required',
    ];

    public function render()
    {
        $userId = auth()->id();
        $ofertas = Oferta_laboral::all();
        $postulacion = Postulacion::where('user_id', $userId)
            ->latest('id')
            ->paginate($this->amount);

        return view('pages.postulaciones', compact('postulacion', 'ofertas'));
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('postulaciones');
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->postulaciones['id'])) {
            $postulacion = Postulacion::create($this->postulaciones);
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $postulacion = Postulacion::find($this->postulaciones['id']);
            $postulacion->update($this->postulaciones);
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'postulaciones']);
        $this->emitTo('TablePostulaciones', 'render');
    }

    public function edit($postulacion)
    {
        $this->postulaciones = array_slice($postulacion, 0, 7);
        $this->isOpen = true;
        $this->ruteCreate = false;
    }

    public function delete($id)
    {
        Postulacion::find($id)->delete();
    }
}

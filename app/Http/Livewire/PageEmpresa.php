<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Empresa;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Livewire\Component;
use Livewire\WithPagination;
use Illuminate\Support\Str;
use Livewire\WithFileUploads;
use Stichoza\GoogleTranslate\GoogleTranslate;

class PageEmpresa extends Component
{
    use WithPagination;
    use WithFileUploads;

    public $isOpen = false, $ruteRegister = true;
    public $empresaState, $empresaCategory, $amount = 5;
    public $search, $empresa;
    public $images = [];
    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'empresa.razon_social' => 'required',
        'empresa.ruc' => 'required',
        'empresa.direccion' => 'required',
        'empresa.telefono' => 'required',
        'empresa.correo' => 'required',
        'empresa.user_id' => 'required',
    ];

    public function render()
    {
        $this->empresa['user_id'] = auth()->user()->id;

        $empresa = Empresa::where('user_id', auth()->user()->id)
            ->latest('id')
            ->paginate($this->amount);
        return view('admin.pages.page-empresa', compact('empresa'));
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteRegister = true;
        $this->reset('empresa');
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->empresa['id'])) {
            $empresa = Empresa::create($this->empresa);
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $empresa = Empresa::find($this->empresa['id']);
            $empresa->update($this->empresa);
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'empresa']);
        $this->emitTo('empresas', 'render');
    }

    public function edit($empresa)
    {
        $this->empresa = array_slice($empresa, 0, 7);
        $this->isOpen = true;
        $this->ruteRegister = false;
    }

    public function delete($id)
    {
        Empresa::find($id)->delete();
    }
}

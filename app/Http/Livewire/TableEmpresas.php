<?php

namespace App\Http\Livewire;

use App\Models\Empresa;
use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class TableEmpresas extends Component
{
    use WithPagination;

    public $isOpen = false,
        $ruteCreate = false,
        $isOpenAssign = false,
        $rutaAsignar = false;
    public $empresaState,
        $empresaCategory,
        $amount = 5;
    public $search, $empresa;
    public $images = [];
    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'empresa.razon_social' => 'required',
        'empresa.ruc' => 'required',
        'empresa.direccion' => 'required',
        'empresa.telefono' => 'required',
        'empresa.correo' => 'required',
        'empresa.creador' => 'required',
        'empresa.user_id' => 'nullable',
    ];

    public function render()
    {
        $this->empresa['creador'] = auth()->user()->name;

        // $usuarios = User::all();
        $usuarios = User::whereNotIn('id', function($query) {
            $query->select('user_id')
                  ->from('empresas')
                  ->whereNotNull('user_id')
                  ->distinct();
        })->get();

        $user = auth()->user();

        if ($user->hasRole('Super-Admin')) {
            $empresas = Empresa::where('razon_social', 'like', '%' . $this->search . '%')
                ->latest('id')
                ->paginate($this->amount);
        } elseif ($user->hasRole('Administrador')) {
            $empresas = Empresa::where('user_id', $user->id)
                ->where('razon_social', 'like', '%' . $this->search . '%')
                ->latest('id')
                ->paginate($this->amount);
        }
        return view('admin.pages.table-empresas', compact('empresas', 'usuarios'));
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('empresa');
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->empresa['id'])) {
            $empresa = Empresa::create($this->empresa);
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $empresa = Empresa::find($this->empresa['id']);
            $empresa->update($this->empresa);
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'empresa']);
        $this->emitTo('empresas', 'render');
    }

    public function edit($empresa)
    {
        $this->empresa = array_slice($empresa, 0, 7);
        $this->isOpen = true;
        $this->ruteCreate = false;
    }

    public function delete($id)
    {
        Empresa::find($id)->delete();
    }

    public function asignarUsuario(Empresa $empresa)
    {
        $this->empresa = $empresa;
        $this->isOpenAssign = true;
        $this->rutaAsignar = true;
    }

    public function updateEmpresa($empresaId)
    {
        $empresa = Empresa::findOrFail($empresaId);
        $empresa->update($this->empresa->toArray());
        $this->reset(['isOpenAssign', 'rutaAsignar']);
        $this->emit('alert', 'Se asignó correctamente al usuario');
    }

    public function eliminarAsignacionUsuario($empresaId)
    {
        $empresa = Empresa::findOrFail($empresaId);

        // dd($empresa);

        // Eliminar la asignación de usuario
        $empresa->user_id = null;
        $empresa->update();

        $this->emit('alert', 'Se eliminó correctamente');
    }
}

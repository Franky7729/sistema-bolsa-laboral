<?php

namespace App\Http\Livewire;

use App\Models\Category;
use App\Models\Empresa;
use App\Models\Oferta_laboral;
use App\Models\Postulacion;
use App\Models\User;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Stichoza\GoogleTranslate\GoogleTranslate;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\DB;


class TablePostulacion extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $isOpen = false,
        $ruteCreate = false;
    public $ofertaState,
        $ofertaCategory,
        $amount = 5;
    public $search, $oferta, $image;
    public $ofertaId;
    public $postulaciones;
    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'postulaciones.user_id' => 'required',
        'postulaciones.oferta_laboral_id' => 'required',
        'postulaciones.fecha_hora_postulacion' => 'required',
        'postulaciones.tipo' => 'required',
        'postulaciones.seleccionado' => 'required',
    ];

    public function render()
    {
        $ofertas = Oferta_laboral::all();

        // Obtener el ID del usuario actualmente autenticado
        $userId = auth()->id();

        // Obtener todas las empresas asociadas al usuario autenticado
        $empresas = Empresa::where('user_id', $userId)->pluck('id');

        // Obtener todas las ofertas laborales asociadas a las empresas del usuario autenticado
        $ofertasIds = Oferta_laboral::whereIn('empresa_id', $empresas)->pluck('id');

        // Obtener todas las postulaciones asociadas a las ofertas laborales del usuario autenticado
        $postulacion = Postulacion::whereHas('oferta_laboral', function ($query) use ($ofertasIds) {
            $query->whereIn('id', $ofertasIds)->where('titulo', 'like', '%' . $this->search . '%');
        })
            ->latest('id')
            ->paginate($this->amount);

        return view('admin.pages.table-postulaciones', compact('postulacion', 'ofertas'));
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('postulaciones');
    }

    public function store()
    {
        $this->validate();

        // Iniciar una transacción para asegurar la integridad de los datos
        DB::beginTransaction();
        try {
            if (!isset($this->postulaciones['id'])) {
                $postulacion = Postulacion::create($this->postulaciones);
                $this->emit('alert', 'Registro creado satisfactoriamente');
            } else {
                $postulacion = Postulacion::find($this->postulaciones['id']);
                // Actualizar la postulación actual
                $postulacion->update($this->postulaciones);

                // Si el estado seleccionado es 1 (seleccionado), actualizar el resto de postulaciones
                if ($this->postulaciones['seleccionado'] == 1) {
                    // Obtener todas las postulaciones de la misma oferta laboral, excepto la actual
                    Postulacion::where('oferta_laboral_id', $postulacion->oferta_laboral_id)
                        ->where('id', '!=', $postulacion->id)
                        ->update(['seleccionado' => 2]); // Establecer a 'no seleccionado'
                }

                $this->emit('alert', 'Registro actualizado satisfactoriamente');
            }

            DB::commit();
            $this->reset(['isOpen', 'postulaciones']);
            $this->emitTo('TablePostulaciones', 'render');
        } catch (\Exception $e) {
            // Revertir todas las operaciones en caso de error
            DB::rollBack();
            $this->emit('alert', 'Error al actualizar los registros: ' . $e->getMessage());
        }
    }



    public function edit($postulacion)
    {
        $this->postulaciones = array_slice($postulacion, 0, 7);
        $this->isOpen = true;
        $this->ruteCreate = false;
    }

    public function delete($id)
    {
        Postulacion::find($id)->delete();
    }
}

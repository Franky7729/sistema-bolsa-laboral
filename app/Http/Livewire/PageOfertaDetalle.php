<?php

namespace App\Http\Livewire;

use App\Models\Oferta_laboral;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Route;

class PageOfertaDetalle extends Component
{
    use WithFileUploads;

    public $isOpen = false;
    public $ruteCreate = false;
    public $amount = 5;
    public $search, $oferta, $image, $ofertaId;
    protected $listeners = ['render', 'delete' => 'delete'];

    public function render()
    {
        $this->ofertaId = Route::current()->parameter('ofertaId');
        $ofertaDetalle = Oferta_laboral::findOrFail($this->ofertaId);

        return view('pages.oferta-detalles', compact('ofertaDetalle'));
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('oferta', 'image');
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->oferta['id'])) {
            $oferta = Oferta_laboral::create($this->oferta);
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                $oferta->image()->create([
                    'url' => $image,
                ]);
            }
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $oferta = Oferta_laboral::find($this->oferta['id']);
            // $oferta->update($this->oferta);
            $oferta->update(Arr::except($this->oferta, 'image'));
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                if ($oferta->image) {
                    Storage::disk('public')->delete('galery', $oferta->image->url);
                    $oferta->image()->update([
                        'url' => $image,
                    ]);
                } else {
                    $oferta->image()->create([
                        'url' => $image,
                    ]);
                }
            }
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'oferta', 'image']);
        $this->emitTo('TableOfertas', 'render');
    }

    public function edit($oferta)
    {
        $this->oferta = $oferta;
        $this->isOpen = true;
        $this->ruteCreate = false;
    }

    public function delete($id)
    {
        Oferta_laboral::find($id)->delete();
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Features;
use Illuminate\Support\Facades\Hash;

class AccountSettingProfile extends Component
{
    use WithFileUploads;

    public $ruteCreate = false;
    public $photo;
    public $user;
    public $verificationLinkSent = false;
    public $name;
    public $email;
    public $nombres;
    public $apellidos;
    public $direccion;
    public $dni;
    public $telefono;
    public $state = [];
    protected $listeners = ['render', 'delete' => 'delete'];

    public function edit()
    {
        $user = $this->getUser();
        $this->name = $user->name;
        $this->email = $user->email;
        $this->nombres = $user->nombres;
        $this->apellidos = $user->apellidos;
        $this->direccion = $user->direccion;
        $this->dni = $user->dni;
        $this->telefono = $user->telefono;
        $this->ruteCreate = false;
    }

    public function store()
    {
        $this->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'nombres' => 'required|string|max:255',
            'apellidos' => 'required|string|max:255',
            'direccion' => 'required|string|max:255',
            'dni' => 'required|digits:8',
            'telefono' => 'required|digits:9',
        ]);

        $user = $this->getUser();
        $user->update([
            'name' => $this->name,
            'email' => $this->email,
            'nombres' => $this->nombres,
            'apellidos' => $this->apellidos,
            'direccion' => $this->direccion,
            'dni' => $this->dni,
            'telefono' => $this->telefono,
        ]);

        if ($this->photo) {
            $user->updateProfilePhoto($this->photo);
        }

        $this->emit('alert', 'Registro actualizado satisfactoriamente');
        $this->reset(['photo', 'name', 'email', 'nombres', 'apellidos', 'direccion', 'dni', 'telefono']);
        $this->emitTo('AccountSettingProfile', 'render');
        return redirect('/sistema/pagina/configurar-cuenta-perfil');
    }

    public function sendEmailVerification()
    {
        $user = $this->getUser();

        if (!$user->hasVerifiedEmail() && Features::enabled(Features::emailVerification())) {
            $user->sendEmailVerificationNotification();
            $this->verificationLinkSent = true;
        }
    }

    public function deleteProfilePhoto()
    {
        $user = $this->getUser();
        $user->deleteProfilePhoto();
    }

    private function getUser()
    {
        return Auth::user();
    }

    public function render()
    {
        $this->edit();
        $this->user = $this->getUser();
        session()->forget('active_menu');
        return view('admin.pages.account-setting-profile');
    }

    public function updatePasswords()
    {
        $rules = [];

        // Validar la contraseña actual solo si se proporciona
        $rules['state.current_password'] = 'sometimes|required|string';

        // Validar la nueva contraseña solo si se proporciona
        $rules['state.password'] = 'sometimes|required|string|min:8|confirmed';

        // Aplicar las reglas de validación
        $this->validate($rules);

        // Validar la contraseña actual si se proporcionó
        if (!empty($this->state['current_password'])) {
            if (!Hash::check($this->state['current_password'], Auth::user()->password)) {
                $this->addError('state.current_password', __('La contraseña actual es incorrecta.'));
                return;
            }
        }

        // Actualizar la contraseña si se proporcionó y es válida
        if (!empty($this->state['password'])) {
            Auth::user()->update([
                'password' => Hash::make($this->state['password']),
            ]);

            // Emitir el mensaje de contraseña actualizada correctamente
            $this->emit('alert', 'Contraseña actualizada correctamente');
            $this->reset('state');
            $this->emit('saved');
            $this->emitTo('AccountSettingProfile', 'render');
        }
    }
}

<?php

namespace App\Http\Livewire;

use App\Models\Empresa;
use App\Models\Oferta_laboral;
use App\Models\Postulacion;
use App\Models\User;
use Illuminate\Support\Arr;
use Livewire\Component;
use Livewire\WithPagination;
use Livewire\WithFileUploads;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;

class TableOfertas extends Component
{
    use WithFileUploads;
    use WithPagination;

    public $isOpen = false,
        $ruteCreate = false;
    public $ofertaState,
        $ofertaCategory,
        $amount = 10;
    public $search, $oferta, $image;

    public $isOpenUserList = false;
    public $userss = [];

    public $postu;
    protected $listeners = ['render', 'delete' => 'delete'];

    protected $rules = [
        'oferta.titulo' => 'required',
        'oferta.descripcion' => 'required',
        'oferta.fecha_publicacion' => 'required',
        'oferta.fecha_cierre' => 'required',
        'oferta.remuneracion' => 'required',
        'oferta.ubicacion' => 'required',
        'oferta.tipo' => 'required',
        'oferta.cantidad' => 'required',
        'oferta.empresa_id' => 'required',
    ];

    public function render()
    {
        // Obtener el usuario autenticado
        $user = Auth::user();
        $empresa = Empresa::where('user_id', $user->id)->first();
        $empresaId = $empresa->id ?? null;

        // Asignar el ID de la empresa a la oferta
        $this->oferta['empresa_id'] = $empresaId;

        // Obtener el ID del usuario actualmente autenticado
        $userId = auth()->id();

        // Obtener todas las empresas asociadas al usuario autenticado
        $empresas = Empresa::where('user_id', $userId)->pluck('id');

        // Obtener todas las ofertas laborales asociadas a estas empresas
        $ofertas = Oferta_laboral::whereIn('empresa_id', $empresas)
            ->where('titulo', 'like', '%' . $this->search . '%')
            ->latest('id')
            ->paginate($this->amount);

        $postulaciones = Postulacion::all();

        // Crear un array asociativo para almacenar los usuarios por oferta laboral
        $usuariosPorOferta = [];

        // Para cada oferta laboral, obtener los usuarios relacionados y almacenarlos en el array
        foreach ($ofertas as $oferta) {
            $usuariosPorOferta[$oferta->id] = $this->mostrarListaUsuarios($oferta->id);
        }


        return view('admin.pages.table-ofertas', compact('ofertas', 'postulaciones', 'usuariosPorOferta'));
    }

    public function mostrarListaUsuarios($offerId)
    {

        // $this->isOpenUserList = true;

        // $this->userss = User::all();

        $postulaciones = Postulacion::where('oferta_laboral_id', $offerId)->get();
        $userIds = $postulaciones->pluck('user_id');
        $users = User::whereIn('id', $userIds)->get();
        return $users;
    }

    public function create()
    {
        $this->isOpen = true;
        $this->ruteCreate = true;
        $this->reset('oferta', 'image');
        $this->resetValidation();
    }

    public function store()
    {
        $this->validate();
        if (!isset($this->oferta['id'])) {
            $oferta = Oferta_laboral::create($this->oferta);
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                $oferta->image()->create([
                    'url' => $image,
                ]);
            }
            $this->emit('alert', 'Registro creado satisfactoriamente');
        } else {
            $oferta = Oferta_laboral::find($this->oferta['id']);
            // $oferta->update($this->oferta);
            $oferta->update(Arr::except($this->oferta, 'image'));
            if ($this->image) {
                $image = Storage::disk('public')->put('galery', $this->image);
                if ($oferta->image) {
                    Storage::disk('public')->delete('galery', $oferta->image->url);
                    $oferta->image()->update([
                        'url' => $image,
                    ]);
                } else {
                    $oferta->image()->create([
                        'url' => $image,
                    ]);
                }
            }
            $this->emit('alert', 'Registro actualizado satisfactoriamente');
        }
        $this->reset(['isOpen', 'oferta', 'image']);
        $this->emitTo('TableOfertas', 'render');
    }

    public function edit($oferta)
    {
        $this->reset('image');
        $this->oferta = array_slice($oferta, 0, 15);
        $this->isOpen = true;
        $this->ruteCreate = false;
    }

    public function delete($id)
    {
        Oferta_laboral::find($id)->delete();
    }
}

<?php

namespace App\Http\Livewire;

use Livewire\Component;

class AccountSettingYape extends Component
{
    public function render()
    {
        session()->forget('active_menu');
        return view('admin.pages.account-setting-yape');
    }
}

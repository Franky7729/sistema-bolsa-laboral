<?php

namespace App\Http\Livewire;

use Livewire\Component;

class PageHome extends Component
{
    public function render()
    {
        return view('livewire.page-home');
    }
}

@props(['active'])

@php
$classes = ($active ?? false)
            ? 'w-full relative px-4 py-2 flex justify-between items-center align-middle rounded-xl text-white bg-blue-700'
            : 'w-full px-4 py-2 flex justify-between items-center align-middle rounded-xl text-zinc-200 group hover:text-white hover:bg-blue-500 active:bg-blue-400';
@endphp

<li>
    <button {{ $attributes->merge(['class' => $classes]) }}>
        <div class="flex space-x-2 items-center">
            <div class="h-6 w-6 flex justify-center items-center">
                {{ $logo }}
            </div>
            <div class="font-medium text-sm text-left w-36 whitespace-nowrap overflow-hidden text-ellipsis">{{ $slot }}</div>
        </div>
        <div class="h-6 w-6 flex justify-center items-center">
            <i class="fa-solid fa-chevron-right fa-xs transition-transform duration-200 transform" :class="{ 'rotate-90': open, 'rotate-0': !open }"></i>
        </div>
    </button>
</li>


<div>
    @if (!Request::is('sistema/pagina/oferta-laboral-detalle/*'))
        @include('admin.partials.menu.verticalMenu')
        <div class="lg:ml-64">
            @include('admin.partials.navbar.navbar')
            <div class="flex flex-col justify-between" style="min-height: calc(100vh - 4rem)">
                <div class="p-6">
                    <div class="mb-4">
                        <span class="text-zinc-500">@yield('header')</span>
                        / <span class="font-semibold">@yield('section')</span>
                    </div>
    @endif

    {{ $content }}

    @if (!Request::is('sistema/pagina/oferta-laboral-detalle/*'))
</div>
<div class="bg-white ">
    @include('admin.partials.footer.footer')
</div>
</div>
</div>
@endif

</div>

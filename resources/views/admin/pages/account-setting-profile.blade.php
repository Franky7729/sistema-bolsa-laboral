@section('title', __('Configurar cuenta - Perfil'))
@section('header', __('Configurar cuenta'))
@section('section', __('Perfil'))

<div>
    <div class="grid grid-cols-1 xl:grid-cols-3 xl:gap-4">

        <div class="col-span-2">
            <div class="p-4 mb-4 bg-white border border-gray-200 rounded-lg shadow-lg">
                <x-form-section submit="store">
                    <x-slot name="title">
                        <h3 class="mb-4 text-xl font-semibold">{{ __('Información general') }}</h3>
                    </x-slot>

                    <x-slot name="form">
                        <!-- Profile Photo -->
                        @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
                            <div x-data="{ photoName: null, photoPreview: null }" class="mb-4 items-center sm:flex 2xl:flex sm:gap-4 2xl:gap-4">
                                <!-- Profile Photo File Input -->
                                <input type="file" id="photo" class="hidden" wire:model.live="photo"
                                    x-ref="photo"
                                    x-on:change="
                                                    photoName = $refs.photo.files[0].name;
                                                    const reader = new FileReader();
                                                    reader.onload = (e) => {
                                                        photoPreview = e.target.result;
                                                    };
                                                    reader.readAsDataURL($refs.photo.files[0]);
                                            " />

                                <!-- Current Profile Photo -->
                                <div x-show="! photoPreview">
                                    @if ($this->user->profile_photo_path)
                                        <img src="{{ Storage::url($this->user->profile_photo_path) }}"
                                            alt="{{ $this->user->name }}" class="rounded-lg w-28 h-28 object-cover">
                                    @else
                                        <img src="{{ $this->user->profile_photo_url }}" alt="{{ $this->user->name }}"
                                            class="rounded-lg w-28 h-28">
                                    @endif

                                </div>

                                <!-- New Profile Photo Preview -->
                                <div x-show="photoPreview" style="display: none;">
                                    <span class="block rounded-lg w-28 h-28  bg-cover"
                                        x-bind:style="'background-image: url(\'' + photoPreview + '\');'">
                                    </span>
                                </div>

                                <div>
                                    <h3 class="mb-1 text-xl font-bold text-gray-900">
                                        {{ __('Foto de perfil') }}</h3>
                                    <div class="mb-4 text-sm text-gray-500">
                                        {{ __('JPG, JPEG o PNG. Tamaño máximo de 1MB') }}
                                    </div>
                                    <div class="flex items-center space-x-4">
                                        <x-button type="button" x-on:click.prevent="$refs.photo.click()"
                                            class="inline-flex items-center">
                                            <svg class="w-4 h-4 mr-2 -ml-1" fill="currentColor" viewBox="0 0 20 20"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M5.5 13a3.5 3.5 0 01-.369-6.98 4 4 0 117.753-1.977A4.5 4.5 0 1113.5 13H11V9.413l1.293 1.293a1 1 0 001.414-1.414l-3-3a1 1 0 00-1.414 0l-3 3a1 1 0 001.414 1.414L9 9.414V13H5.5z">
                                                </path>
                                                <path d="M9 13h2v5a1 1 0 11-2 0v-5z"></path>
                                            </svg>
                                            {{ __('Subir nueva foto') }}
                                        </x-button>
                                        @if ($this->user->profile_photo_path)
                                            <x-button-danger type="button" wire:click="deleteProfilePhoto"
                                                class="">
                                                {{ __('Eliminar foto') }}
                                            </x-button-danger>
                                        @endif

                                        <x-input-error for="photo" class="mt-2" />
                                    </div>
                                </div>
                            </div>
                        @endif

                        <div class="grid grid-cols-6 gap-4">
                            <!-- Name -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('Nombre de usuario') }}" />
                                <x-input type="text" class="mt-1 block w-full" x-ref="name" id="name"
                                    wire:model.defer="name" />
                                <x-input-error for="name" class="mt-2" />
                            </div>

                            <!-- Email -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('Email') }}" />
                                <x-input type="email" class="mt-1 block w-full" x-ref="email" id="email"
                                    wire:model.defer="email" />
                                <x-input-error for="email" class="mt-2" />

                                @if (Laravel\Fortify\Features::enabled(Laravel\Fortify\Features::emailVerification()) &&
                                        !$this->user->hasVerifiedEmail())
                                    <p class="text-sm mt-2 text-red-600">
                                        {{ __('Tu correo electrónico no está verificado.') }}

                                        <button type="button"
                                            class="text-left underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                            wire:click.prevent="sendEmailVerification">
                                            {{ __('Haz clic aquí para reenviar el correo de verificación.') }}
                                        </button>
                                    </p>

                                    @if ($this->verificationLinkSent)
                                        <p class="mt-2 font-medium text-sm text-green-600">
                                            {{ __('Se ha enviado un nuevo enlace de verificación a tu dirección de correo electrónico.') }}
                                        </p>
                                    @endif
                                @endif
                            </div>

                            <!-- Nombres -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('Nombres') }}" />
                                <x-input type="text" class="mt-1 block w-full" x-ref="nombres" id="nombres"
                                    wire:model.defer="nombres" />
                                <x-input-error for="nombres" class="mt-2" />
                            </div>

                            <!-- Apellidos -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('Apellidos') }}" />
                                <x-input type="text" class="mt-1 block w-full" x-ref="apellidos" id="apellidos"
                                    wire:model.defer="apellidos" />
                                <x-input-error for="apellidos" class="mt-2" />
                            </div>

                            <!-- Dirección -->
                            <div class="col-span-6 sm:col-span-6">
                                <x-label value="{{ __('Dirección') }}" />
                                <x-input type="text" class="mt-1 block w-full" x-ref="direccion" id="direccion"
                                    wire:model.defer="direccion" />
                                <x-input-error for="direccion" class="mt-2" />
                            </div>

                            <!-- DNI -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('DNI') }}" />
                                <x-input type="tel" class="mt-1 block w-full" x-ref="dni" id="dni"
                                    wire:model.defer="dni" pattern="[0-9]*" placeholder=" "
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="8" />
                                <x-input-error for="dni" class="mt-2" />
                            </div>

                            <!-- Teléfono -->
                            <div class="col-span-6 sm:col-span-3">
                                <x-label value="{{ __('Teléfono') }}" />
                                <x-input type="tel" class="mt-1 block w-full" x-ref="telefono" id="telefono"
                                    wire:model.defer="telefono" pattern="[0-9]*" placeholder=" "
                                    oninput="this.value = this.value.replace(/[^0-9]/g, '');" maxlength="9" />
                                <x-input-error for="telefono" class="mt-2" />
                            </div>
                        </div>
                    </x-slot>

                    <x-slot name="actions">
                        <x-button wire:click.prevent="store()" wire:loading.attr="disabled"
                            wire:target="store, photo">
                            {{-- {{ __('Save') }} --}}
                            @if ($ruteCreate)
                                Registrar
                            @else
                                {{ __('Save') }}
                            @endif
                        </x-button>
                        <x-action-message class="ms-3" on="saved">
                            {{ __('Saved') }}
                        </x-action-message>
                    </x-slot>
                </x-form-section>
            </div>

            <div class="p-4 mb-4 bg-white border border-gray-200 rounded-lg shadow-lg">
                <x-form-section submit="updatePasswords">

                    <x-slot name="title">
                        <h3 class="mb-4 text-xl font-semibold">{{ __('Información de contraseña') }}</h3>
                    </x-slot>

                    <x-slot name="form">
                        <div class="grid grid-cols-6 gap-4">
                            <div class="col-span-6 sm:col-span-4">
                                <x-label for="current_password" value="{{ __('Contraseña actual') }}" />
                                <x-input id="current_password" type="password" placeholder="••••••••"
                                    class="mt-1 block w-full" wire:model.defer="state.current_password"
                                    autocomplete="current-password" />
                                <x-input-error for="current_password" class="mt-2" />
                            </div>


                            <div class="col-span-6 sm:col-span-3">
                                <x-label for="password" value="{{ __('Nueva contraseña') }}" />
                                <input data-popover-target="popover-password" data-popover-placement="bottom"
                                    type="password" id="password" wire:model.defer="state.password"
                                    autocomplete="new-password"
                                    class="mt-1 block w-full border-gray-300 focus:border-indigo-500 focus:ring-indigo-500 rounded-md shadow-sm"
                                    placeholder="••••••••" required="">
                                <div data-popover="" id="popover-password" role="tooltip"
                                    class="absolute z-10 inline-block text-sm font-light text-gray-500 transition-opacity duration-300 bg-white border border-gray-200 rounded-lg shadow-sm w-72 opacity-0 invisible"
                                    style="position: absolute; inset: auto auto 0px 0px; margin: 0px; transform: translate(520px, -1768px);"
                                    data-popper-reference-hidden="" data-popper-escaped=""
                                    data-popper-placement="top">
                                    <div class="p-3 space-y-2">
                                        <h3 class="font-semibold text-gray-900 dark:text-white">Must have at least 6
                                            characters</h3>
                                        <div class="grid grid-cols-4 gap-2">
                                            <div class="h-1 bg-orange-300 dark:bg-orange-400"></div>
                                            <div class="h-1 bg-orange-300 dark:bg-orange-400"></div>
                                            <div class="h-1 bg-gray-200 dark:bg-gray-600"></div>
                                            <div class="h-1 bg-gray-200 dark:bg-gray-600"></div>
                                        </div>
                                        <p>It’s better to have:</p>
                                        <ul>
                                            <li class="flex items-center mb-1">
                                                <svg class="w-4 h-4 mr-2 text-green-400 dark:text-green-500"
                                                    aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                        d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z"
                                                        clip-rule="evenodd"></path>
                                                </svg>
                                                Upper &amp; lower case letters
                                            </li>
                                            <li class="flex items-center mb-1">
                                                <svg class="w-4 h-4 mr-2 text-gray-300 dark:text-gray-400"
                                                    aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                        clip-rule="evenodd"></path>
                                                </svg>
                                                A symbol (#$&amp;)
                                            </li>
                                            <li class="flex items-center">
                                                <svg class="w-4 h-4 mr-2 text-gray-300 dark:text-gray-400"
                                                    aria-hidden="true" fill="currentColor" viewBox="0 0 20 20"
                                                    xmlns="http://www.w3.org/2000/svg">
                                                    <path fill-rule="evenodd"
                                                        d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                                        clip-rule="evenodd"></path>
                                                </svg>
                                                A longer password (min. 12 chars.)
                                            </li>
                                        </ul>
                                    </div>
                                    <div data-popper-arrow=""
                                        style="position: absolute; left: 0px; transform: translate(138.667px, 0px);">
                                    </div>
                                </div>
                                <x-input-error for="password" class="mt-2" />
                            </div>

                            <div class="col-span-6 sm:col-span-3">
                                <x-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                                <x-input id="password_confirmation" type="password" placeholder="••••••••"
                                    class="mt-1 block w-full" wire:model.defer="state.password_confirmation"
                                    autocomplete="new-password" />
                                <x-input-error for="password_confirmation" class="mt-2" />
                            </div>
                        </div>
                    </x-slot>

                    <x-slot name="actions">
                        <x-button>
                            {{ __('Save') }}
                        </x-button>

                        <x-action-message class="ms-3" on="saved">
                            {{ __('Saved') }}
                        </x-action-message>
                    </x-slot>
                </x-form-section>
            </div>
        </div>

        <div class="col-span-full xl:col-auto">
            <div class="p-4 mb-4 bg-white border border-gray-200 rounded-lg shadow-lg 2xl:col-span-2">
                @if (Laravel\Fortify\Features::canManageTwoFactorAuthentication())
                    @livewire('profile.two-factor-authentication-form')
                @endif
            </div>

            <div class="p-4 mb-4 bg-white border border-gray-200 rounded-lg shadow-lg 2xl:col-span-2">
                @livewire('profile.logout-other-browser-sessions-form')
            </div>
        </div>
    </div>
</div>

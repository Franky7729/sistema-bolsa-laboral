@section('title', 'Tabla - Empresas')
@section('header', 'Tabla')
@section('section', 'Empresas')

<div>
    <div>
        <x-card>
            <form autocomplete="off">
                <div class="grid grid-cols-6 gap-4">
                    <!-- Name -->
                    <div class="col-span-6 sm:col-span-3">
                        <x-label value="{{ __('RUC') }}" />
                        <x-input type="number" class="mt-1 block w-full" wire:model="empresa.ruc" required />
                        <x-input-error for="empresa.ruc" class="mt-2" />
                    </div>

                    <!-- Email -->
                    <div class="col-span-6 sm:col-span-3">
                        <x-label value="{{ __('Razon social') }}" />
                        <x-input type="text" class="mt-1 block w-full" wire:model="empresa.razon_social" required />
                        <x-input-error for="empresa.razon_social" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-3">
                        <x-label value="{{ __('Correo') }}" />
                        <x-input type="text" class="mt-1 block w-full" wire:model="empresa.correo" required />
                        <x-input-error for="empresa.correo" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-3">
                        <x-label value="{{ __('Direccion') }}" />
                        <x-input type="text" class="mt-1 block w-full" wire:model="empresa.direccion" required />
                        <x-input-error for="empresa.direccion" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-3">
                        <x-label value="{{ __('Telefono') }}" />
                        <x-input type="text" class="mt-1 block w-full" wire:model="empresa.telefono" required />
                        <x-input-error for="empresa.telefono" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-2">
                        <x-label value="{{ __('Archivo') }}" />
                        <input
                            class="mt-1 block w-full text-base text-gray-900 border border-gray-300 rounded-md cursor-pointer bg-gray-50"
                            id="file_input" type="file" wire:model="empresa.archivo" required>
                            <div class="h-1 mt-1 bg-blue-700 rounded"></div>
                        <x-input-error for="empresa.archivo" class="mt-2" />
                    </div>
                    <div class="col-span-6 sm:col-span-1">
                        <x-label value="{{ __('Estado') }}" />
                        <div class="border-gray-300 border rounded-md shadow-sm w-full mt-1 py-2 px-3">
                            No enviado
                        </div>
                        <x-input-error for="empresa.estado" class="mt-2" />
                    </div>
                    <input hidden wire:model="empresa.user_id" />
                </div>
                <x-button-success wire:click.prevent="store()" wire:loading.attr="disabled" wire:target="store"
                    class="disabled:opacity-25 mt-4">
                    @if ($ruteRegister)
                        Registrar
                    @else
                        Registrado
                    @endif
                </x-button-success>
            </form>
        </x-card>

    </div>
</div>

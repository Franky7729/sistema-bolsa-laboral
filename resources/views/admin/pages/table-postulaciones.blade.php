@section('title', 'Tabla - Postulaciones')
@section('header', 'Tabla')
@section('section', 'Postulaciones')

<div>
    <div>
        @if ($isOpen)
            @include('admin.modals.postulacion')
        @endif
        <x-card>
            <div class="flex flex-col sm:flex-row sm:justify-between text-center gap-2 mb-4">
                <div class="flex-1">
                    <x-search />
                </div>
                <div x-data="{ open: false }" @click.away="open = false" class="flex flex-wrap justify-center gap-2"
                    align="right">
                    <x-select wire:model="amount" class="w-max">
                        <x-slot name="options">
                            <option value="5" selected>5</option>
                            <option value="10">10</option>
                            <option value="30">30</option>
                            <option value="50">50</option>
                            <option value="70">70</option>
                            <option value="100">100</option>
                        </x-slot>
                    </x-select>

                    <x-button-default @click="open = ! open" id="dropdownBottomButton"
                        data-dropdown-toggle="dropdownBottom" data-dropdown-placement="bottom" type="button">
                        <i class="fa-solid fa-file-export me-1"></i>
                        {{ __('Exportar') }}
                        <i class="fa-solid fa-chevron-right ms-2 transition-transform duration-200 transform"
                            :class="{ 'rotate-90': open, 'rotate-0': !open }"></i>
                    </x-button-default>

                    <!-- Dropdown menu -->
                    <div id="dropdownBottom" class="z-10 hidden bg-white rounded-lg shadow-lg w-40 border">
                        <div class="py-2 text-sm font-medium text-zinc-700" aria-labelledby="dropdownBottomButton">
                            <x-button-hover color="green" wire:click="createCSV()" target="_blank">
                                <i class="fa-solid fa-file-csv"></i> CSV
                            </x-button-hover>
                            <x-button-hover color="green" wire:click="createExcel()" target="_blank">
                                <i class="fa-solid fa-file-excel me-1"></i> EXCEL
                            </x-button-hover>
                            <a href="{{ URL::to('/empresas/pdf') }}" target="_blank">
                                <x-button-hover color="blue">
                                    <i class="fa-solid fa-file-pdf"></i> PDF
                                </x-button-hover>
                            </a>
                        </div>
                    </div>
                    <x-button wire:click="create()">
                        <i class="fa-solid fa-plus me-1"></i>
                        {{ __('Agregar empresa') }}
                    </x-button>
                </div>
            </div>
            <div class="shadow border-b border-gray-200 rounded-lg overflow-auto">
                <table class="w-full table-auto">
                    <thead class="bg-indigo-600 text-white">
                        <tr class="text-center text-xs font-bold uppercase">
                            <td scope="col" class="px-6 py-3">Usuario</td>
                            <td scope="col" class="px-6 py-3">Oferta laboral</td>
                            <td scope="col" class="px-6 py-3">Fecha de postulacion</td>
                            <td scope="col" class="px-6 py-3">Tipo</td>
                            <td scope="col" class="px-6 py-3">Seleccionado</td>
                            <td scope="col" class="px-6 py-3">CV</td>
                            <td scope="col" class="px-6 py-3">Creado</td>
                            <td scope="col" class="px-6 py-3">Actualizado</td>
                            <th scope="col" class="px-4 py-3 acciones"></th>
                        </tr>
                    </thead>
                    <tbody class="divide-y divide-gray-300 bg-white">
                        @foreach ($postulacion as $index => $postulacio)
                            <tr class="text-sm font-medium text-gray-900 hover:bg-gray-100">
                                <td class="px-4 py-2 text-center">{{ $postulacio->user->name }}</td>
                                <td class="px-4 py-2 text-center">{{ $postulacio->oferta_laboral->titulo }}</td>
                                <td class="px-4 py-2 text-center">{{ $postulacio->fecha_hora_postulacion }}</td>
                                <td class="px-4 py-2 text-center">{{ $postulacio->tipo }}</td>
                                <td class="px-4 py-2 text-center">
                                    <div class="flex justify-center items-center">
                                        @if ($postulacio->seleccionado == 1)
                                            <span class="bg-green-500 text-white text-xs font-semibold px-2.5 py-0.5 rounded-full mb-1">
                                                {{ __('Seleccionado') }}
                                            </span>
                                        @elseif ($postulacio->seleccionado == 2)
                                            <span class="bg-red-600 text-white text-xs font-semibold px-2.5 py-0.5 rounded-full mb-1">
                                                {{ __('No seleccionado') }}
                                            </span>
                                        @else
                                            <span class="bg-yellow-500 text-white text-xs font-semibold px-2.5 py-0.5 rounded-full mb-1">
                                                {{ __('Pendiente') }}
                                            </span>
                                        @endif
                                    </div>
                                </td>
                                <td class="px-4 py-2 text-center">
                                    @if ($postulacio->ruta_pdf)
                                        <a href="{{ Storage::url($postulacio->ruta_pdf) }}" target="_blank" class="text-blue-500 hover:underline">
                                            Ver PDF
                                        </a>
                                    @else
                                        <span class="text-gray-500">N/A</span>
                                    @endif
                                </td>
                                <td class="px-4 py-2 text-center">{{ $postulacio->created_at }}</td>
                                <td class="px-4 py-2 text-center">{{ $postulacio->updated_at }}</td>
                                <td class="px-4 py-2 acciones space-y-2 w-10">
                                    <x-button-success wire:click="edit({{ $postulacio }})">
                                        <i class="fa-regular fa-pen-to-square"></i>
                                    </x-button-success>
                                    <x-button-danger wire:click="$emit('deleteItem',{{ $postulacio->id }})">
                                        <i class="fa-regular fa-trash-can"></i>
                                    </x-button-danger>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            @if ($postulacion->count() == 0 && $search == '')
                <div class="text-center pt-4">
                    <span class="text-slate-600 mt-2 text-sm">Ningún dato disponible en esta tabla</span>
                </div>
            @elseif ($postulacion->count() == 0 && $search != '')
                <div class="flex h-auto items-center justify-center p-5 bg-white w-full rounded-lg shadow-lg">
                    <div class="text-center">
                        <div class="inline-flex rounded-full bg-yellow-100 p-4">
                            <div class="rounded-full text-yellow-600 bg-yellow-200 p-4 text-6xl">
                                <i class="fa-solid fa-circle-exclamation"></i>
                            </div>
                        </div>
                        <h1 class="mt-5 text-2xl font-bold text-slate-800">Ups... algo salio mal</h1>
                        <p class="text-slate-600 mt-2 text-base">No existe ningun registro coincidente con la busqueda
                        </p>
                        <span class="text-slate-600 mt-2 text-base">Por favor ingrese el texto correctamente</span>
                    </div>
                </div>
            @endif
            @if ($postulacion->hasPages())
                <div class="mt-4">
                    {{ $postulacion->links() }}
                </div>
            @endif
        </x-card>

    </div>

    <!--Scripts - Sweetalert   -->
    @push('js')
        <script>
            function next(index) {
                let sliderContainer = document.getElementById('sliderContainer_' + index);
                let slider = document.getElementById('slider_' + index);
                let cards = slider.getElementsByTagName('li');

                let elementsToShow = 1;
                let sliderContainerWidth = sliderContainer.clientWidth;
                let cardWidth = sliderContainerWidth / elementsToShow;

                let marginLeft = +slider.style.marginLeft.slice(0, -2);
                let maxMarginLeft = -cardWidth * (cards.length - elementsToShow);

                if (marginLeft > maxMarginLeft) {
                    slider.style.marginLeft = (marginLeft - cardWidth) + 'px';
                } else {
                    slider.style.marginLeft = '0px';
                }
            }

            function prev(index) {
                let sliderContainer = document.getElementById('sliderContainer_' + index);
                let slider = document.getElementById('slider_' + index);
                let cards = slider.getElementsByTagName('li');

                let elementsToShow = 1;
                let sliderContainerWidth = sliderContainer.clientWidth;
                let cardWidth = sliderContainerWidth / elementsToShow;

                let marginLeft = +slider.style.marginLeft.slice(0, -2);

                if (marginLeft < 0) {
                    slider.style.marginLeft = (marginLeft + cardWidth) + 'px';
                } else {
                    let maxMarginLeft = -cardWidth * (cards.length - elementsToShow);
                    slider.style.marginLeft = maxMarginLeft + 'px';
                }
            }
        </script>

        <script>
            Livewire.on('deleteItem', id => {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: "text-sm text-white ml-1 px-3 py-2 bg-gradient-to-r from-emerald-700 to-green-600 rounded-lg active:ring-2 active:ring-green-500 active:ring-offset-2 transition ease-in-out duration-150",
                        cancelButton: "text-sm text-white mr-1 px-3 py-2 bg-gradient-to-r from-red-700 to-red-600 rounded-lg active:ring-2 active:ring-red-500 active:ring-offset-2 transition ease-in-out duration-150"
                    },
                    buttonsStyling: false
                });
                swalWithBootstrapButtons.fire({
                    title: '¿Estas seguro?',
                    text: "¡No podrás revertir esto!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: "¡Si, bórralo!",
                    cancelButtonText: "¡No, cancelar!",
                    reverseButtons: true
                }).then((result) => {
                    if (result.isConfirmed) {
                        Livewire.emitTo('table-postulacion', 'delete', id);
                        swalWithBootstrapButtons.fire({
                            title: "¡Eliminado!",
                            text: "Su archivo ha sido eliminado correctamente.",
                            icon: "success",
                        });
                    }
                });
            });
        </script>
    @endpush
</div>

<div>
    <x-dialog-modal wire:model="isOpenAssign" maxWidth="lg">
        <x-slot name="title">
            <h3 class="text-center">Asignar usuario</h3>
        </x-slot>
        <x-slot name="content">
            <form autocomplete="off">
                <input type="hidden" wire:model="user.id">
                <div class="flex gap-2.5 w-full px-2 items-center">
                    <x-label value="EMPRESA:" class="font-bold" />
                    <x-input disabled wire:model="empresa.razon_social" />
                </div>
                <div class="w-full px-2 mt-4">
                    <div class="flex-auto">
                        <x-label value="Usuario" class="font-bold" />
                        <x-select wire:model="empresa.user_id">
                            <x-slot name="options">
                                @if ($empresa['user_id'] == null)
                                    <option value="" selected>Seleccione...</option>
                                    @foreach ($usuarios as $user)
                                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                                    @endforeach
                                @else
                                    <option value="{{ $empresa['user_id'] }}" selected>
                                        {{ $usuarios->firstWhere('id', $empresa['user_id'])->name }}
                                    </option>
                                    @foreach ($usuarios as $user)
                                        @if ($user->id !== $empresa['user_id'])
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endif
                                    @endforeach
                                @endif
                            </x-slot>
                        </x-select>
                        @unless (!empty($empresa['user_id']))
                            <x-input-error for="empresa.user_id" />
                        @endunless
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="footer">
            <x-button-danger wire:click="$set('isOpenAssign',false)">Cancelar</x-button-danger>
            <x-button-success wire:click.prevent="updateEmpresa({{ $empresa->id }})" wire:loading.attr="disabled"
                wire:target="store" class="disabled:opacity-25">
                Asignar Usuario
            </x-button-success>
        </x-slot>

    </x-dialog-modal>
</div>

<div>
    <x-dialog-modal wire:model="isOpen" maxWidth="lg">
        <x-slot name="title">
            @if ($ruteCreate)
                <h3 class="text-center">Registrar nueva empresa</h3>
            @else
                <h3 class="text-center">Actualizar empresa</h3>
            @endif
        </x-slot>
        <x-slot name="content">
            <form autocomplete="off">
                <input type="hidden" wire:model="empresa.id">
                <div class="flex flex-col sm:flex-row gap-2.5 w-full px-2">
                    <div class="flex flex-col gap-2.5 w-full">
                        <div class="mb-1">
                            <x-label value="RUC" class="font-bold" />
                            <x-input type="text" wire:model="empresa.ruc" />
                            @unless (!empty($empresa['ruc']))
                                <x-input-error for="empresa.ruc" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Razon social" class="font-bold" />
                            <x-input type="text" wire:model="empresa.razon_social" />
                            @unless (!empty($empresa['razon_social']))
                                <x-input-error for="empresa.razon_social" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Direccion" class="font-bold" />
                            <x-input type="text" wire:model="empresa.direccion" />
                            @unless (!empty($empresa['direccion']))
                                <x-input-error for="empresa.direccion" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Telefono" class="font-bold" />
                            <x-input type="text" wire:model="empresa.telefono" />
                            @unless (!empty($empresa['telefono']))
                                <x-input-error for="empresa.telefono" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Correo" class="font-bold" />
                            <x-input type="text" wire:model="empresa.correo" />
                            @unless (!empty($empresa['correo']))
                                <x-input-error for="empresa.correo" />
                            @endunless
                        </div>
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="footer">
            <x-button-danger wire:click="$set('isOpen',false)">Cancelar</x-button-danger>
            <x-button-success wire:click.prevent="store()" wire:loading.attr="disabled" wire:target="store, image"
                class="disabled:opacity-25">
                @if ($ruteCreate)
                    Registrar
                @else
                    Actualizar
                @endif
            </x-button-success>
        </x-slot>

    </x-dialog-modal>
</div>


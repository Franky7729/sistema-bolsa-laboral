<div>
    <x-dialog-modal wire:model="isOpen" maxWidth="lg">
        <x-slot name="title">
            @if ($ruteCreate)
                <h3 class="text-center">Registrar nueva postulaciones</h3>
            @else
                <h3 class="text-center">Actualizar postulaciones</h3>
            @endif
        </x-slot>
        <x-slot name="content">
            <form autocomplete="off">
                <input type="hidden" wire:model="postulaciones.id">
                <div class="flex flex-col sm:flex-row gap-2.5 w-full px-2">
                    <div class="flex flex-col gap-2.5 w-full">

                        <div class="flex flex-col sm:flex-row gap-2.5">
                            <input hidden wire:model="postulaciones.user_id" />
                        </div>

                        <div class="w-full md:w-12/12 px-3 md:mb-0">
                            <label for="provincia" class="block">
                                Provincia*
                            </label>
                            {{-- @dd($postulaciones['oferta_laboral_id']); --}}
                            <select wire:model="postulaciones.oferta_laboral_id" id="oferta_laboral"
                                class="appearance-none block w-full rounded-xl text-black border border-gray-400 py-2 px-2 mb-3 leading-tight focus:outline-none focus:bg-white">
                                @foreach ($ofertas as $oferta)
                                    <option value="{{ $oferta->id }}"
                                        {{ $postulaciones['oferta_laboral_id'] == $oferta->id ? 'selected' : '' }}>
                                        {{ $oferta->titulo }}

                                    </option>
                                @endforeach
                            </select>
                            @unless (!empty($entrega['provincia']['id']))
                                <x-input-error for="postulaciones.oferta_laboral_id" />
                            @endunless
                        </div>

                        <div>
                            <x-label value="Fecha de postulacion" class="font-bold" />
                            <x-input type="text" wire:model="postulaciones.fecha_hora_postulacion" />
                            @unless (!empty($postulaciones['fecha_hora_postulacion']))
                                <x-input-error for="postulaciones.fecha_hora_postulacion" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Tipo" class="font-bold" />
                            <x-input type="text" wire:model="postulaciones.tipo" />
                            @unless (!empty($postulaciones['tipo']))
                                <x-input-error for="postulaciones.tipo" />
                            @endunless
                        </div>
                        <div class="flex-auto">
                            <x-label value="Seleccionado" class="font-bold" />
                            <x-select wire:model="postulaciones.seleccionado" class="cursor-pointer">
                                <x-slot name="options">
                                    @if ($ruteCreate)
                                        <option value="" selected>Seleccione...</option>
                                        <option value="1">Seleccionado</option>
                                        <option value="2">No seleccionado</option>
                                    @else
                                        @if ($postulaciones['seleccionado'] == 1)
                                            <option value="{{ $postulaciones['seleccionado'] }}" selected>Seleccionado</option>
                                            <option value="2">No seleccionado</option>
                                        @else
                                            <option value="{{ $postulaciones['seleccionado'] }}" selected>No seleccionado</option>
                                            <option value="1">Seleccionado</option>
                                        @endif
                                    @endif
                                </x-slot>
                            </x-select>
                            @unless (!empty($postulaciones['seleccionado']))
                                <x-input-error for="postulaciones.seleccionado" />
                            @endunless
                        </div>

                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="footer">
            <x-button-danger wire:click="$set('isOpen',false)">Cancelar</x-button-danger>
            <x-button-success wire:click.prevent="store()" wire:loading.attr="disabled" wire:target="store"
                class="disabled:opacity-25">
                @if ($ruteCreate)
                    Registrar
                @else
                    Actualizar
                @endif
            </x-button-success>
        </x-slot>
    </x-dialog-modal>
</div>


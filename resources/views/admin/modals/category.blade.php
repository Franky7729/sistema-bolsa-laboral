<div>
    <x-dialog-modal wire:model="isOpen" maxWidth="lg">
        <x-slot name="title">
            @if ($ruteCreate)
                <h3 class="text-center">Registrar nueva categoria</h3>
            @else
                <h3 class="text-center">Actualizar categoria</h3>
            @endif
        </x-slot>
        <x-slot name="content">
            <form autocomplete="off">
                <input type="hidden" wire:model="category.id">
                <div class="flex flex-col sm:flex-row gap-2.5 w-full px-2">
                    <div class="flex flex-col gap-2.5 w-full">
                        <div class="mb-1">
                            <x-label value="Nombre" class="font-bold" />
                            <x-input type="text" wire:model="category.name" />
                            @unless (!empty($category['name']))
                                <x-input-error for="category.name" />
                            @endunless
                        </div>
                        <div>
                            <x-label value="Slug" class="font-bold" />
                            <x-input type="text" wire:model="category.slug" disabled />
                            @unless (!empty($category['slug']))
                                <x-input-error for="category.slug" />
                            @endunless
                        </div>
                        <div class="flex flex-col sm:flex-row gap-2.5">
                            <div class="flex-auto">
                                <x-label value="Estado" class="font-bold" />
                                <x-select wire:model="category.state">
                                    <x-slot name="options">
                                        @if ($ruteCreate)
                                            <option value="" selected>Seleccione...</option>
                                            <option value="1">Escondido</option>
                                            <option value="2">Visible</option>
                                        @else
                                            @if ($category['state'] == 1)
                                                <option value="{{ $category['state'] }}" selected>Escondido</option>
                                                <option value="2">Visible</option>
                                            @else
                                                <option value="{{ $category['state'] }}" selected>Visible</option>
                                                <option value="1">Escondido</option>
                                            @endif
                                        @endif
                                    </x-slot>
                                </x-select>
                                @unless (!empty($category['state']))
                                    <x-input-error for="category.state" />
                                @endunless
                            </div>
                            <input hidden wire:model="category.user" />
                        </div>
                    </div>
                </div>
            </form>
        </x-slot>
        <x-slot name="footer">
            <x-button-danger wire:click="$set('isOpen',false)">Cancelar</x-button-danger>
            <x-button-success wire:click.prevent="store()" wire:loading.attr="disabled" wire:target="store, image"
                class="disabled:opacity-25">
                @if ($ruteCreate)
                    Registrar
                @else
                    Actualizar
                @endif
            </x-button-success>
        </x-slot>

    </x-dialog-modal>
</div>



<x-guest-layout>
    <div class="relative bg-zinc-100">
        <div class="absolute w-full top-0 left-0">
            <div class="flex gap-2">
                <div class="flex-1 bg-green-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-blue-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-yellow-500 h-1 rounded-md"></div>
                <div class="flex-1 bg-red-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-gray-700 h-1 rounded-md"></div>
            </div>
        </div>
        <section class="relative container mx-auto p-14 min-h-screen flex items-center justify-center">
            <div class="h-full">
                <!-- Left column container with background-->
                <div class="flex h-full flex-wrap items-center justify-center lg:justify-between">
                    <div class="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
                        <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/authentication/illustration.svg"
                            class="w-full" alt="Sample image" />
                    </div>

                    <!-- Right column container -->
                    <div class="md:mb-0 md:w-8/12 lg:w-5/12 xl:w-6/12">
                        <div class="w-full bg-white rounded-xl border shadow-lg" style="z-index: 1;">
                            <div class="p-6 space-y-4">
                                <div
                                    class="text-xl flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-indigo-300 after:mt-0.5 after:flex-1 after:border-t after:border-indigo-300">
                                    <div class="flex items-center justify-center mx-4">
                                        <a href="#">
                                            <div class="flex gap-4 items-center text-indigo-600">
                                                <i class="fa-solid fa-briefcase text-3xl"></i>
                                                {{-- <img src="{{ asset('assets/img/favicon/favicon.ico') }}" alt class="w-12 ml-[-10px]"> --}}
                                                <div class="flex flex-col">
                                                    <span class="text-base font-semibold"> Bienvenido a
                                                        la</span>
                                                    <span class="text-sm italic -mt-1"> BOLSA LABORAL </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="text-sm font-normal text-center">
                                    Por favor, ingrese sus datos
                                </div>
                                <x-validation-errors />

                                <form method="POST" action="{{ route('register_user') }}">
                                    @csrf

                                    <div>
                                        <x-label for="name" value="{{ __('Nombre de usuario') }}" />
                                        <x-input id="name" class="block mt-1 w-full" type="text"
                                            name="name" :value="old('name')" required autofocus autocomplete="name" />
                                    </div>


                                    <div class="flex gap-4 mt-4">
                                        <div class="w-full">
                                            <x-label for="nombres" value="{{ __('Nombres') }}" />
                                            <x-input id="nombres" class="block mt-1 w-full" type="text"
                                                name="nombres" :value="old('nombres')" required />
                                        </div>

                                        <div class="w-full">
                                            <x-label for="apellidos" value="{{ __('Apellidos') }}" />
                                            <x-input id="apellidos" class="block mt-1 w-full" type="text"
                                                name="apellidos" :value="old('apellidos')" required />
                                        </div>
                                    </div>


                                    <div class="mt-4">
                                        <x-label for="email" value="{{ __('Email') }}" />
                                        <x-input id="email" class="block mt-1 w-full" type="email"
                                            name="email" :value="old('email')" required autocomplete="username" />
                                    </div>

                                    <div class="flex gap-4 mt-4">
                                        <div class="w-full">
                                            <x-label for="password" value="{{ __('Password') }}" />
                                            <x-input id="password" class="mt-1" type="password"
                                                name="password" required autocomplete="new-password" />
                                        </div>

                                        <div class="w-full">
                                            <x-label for="password_confirmation" value="{{ __('Confirm Password') }}" />
                                            <x-input id="password_confirmation" class="mt-1" type="password"
                                                name="password_confirmation" required autocomplete="new-password" />
                                        </div>
                                    </div>

                                    <div class="flex gap-4 mt-4">
                                        <div class="w-full">
                                            <x-label for="dni" value="{{ __('DNI') }}" />
                                            <x-input id="dni" class="block mt-1 w-full" type="text"
                                                name="dni" :value="old('dni')" required />
                                        </div>

                                        <div class="w-full">
                                            <x-label for="telefono" value="{{ __('Teléfono') }}" />
                                            <x-input id="telefono" class="block mt-1 w-full" type="text"
                                                name="telefono" :value="old('telefono')" required />
                                        </div>
                                    </div>

                                    <div class="mt-4">
                                        <x-label for="direccion" value="{{ __('Dirección') }}" />
                                        <x-input id="direccion" class="block mt-1 w-full" type="text"
                                            name="direccion" :value="old('direccion')" required />
                                    </div>

                                    @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
                                        <div class="mt-4">
                                            <x-label for="terms">
                                                <div class="flex items-center">
                                                    <x-checkbox name="terms" id="terms" required />

                                                    <div class="ml-2">
                                                        {!! __('Acepto los :terms_of_service y :privacy_policy', [
                                                            'terms_of_service' =>
                                                                '<a target="_blank" href="' .
                                                                route('terms.show') .
                                                                '" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">' .
                                                                __('Términos de servicio') .
                                                                '</a>',
                                                            'privacy_policy' =>
                                                                '<a target="_blank" href="' .
                                                                route('policy.show') .
                                                                '" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">' .
                                                                __('Política de privacidad') .
                                                                '</a>',
                                                        ]) !!}
                                                    </div>
                                                </div>
                                            </x-label>
                                        </div>
                                    @endif

                                    <div class="flex items-center justify-end mt-4">
                                        <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                            href="{{ route('login') }}">
                                            {{ __('¿Ya estás registrado?') }}
                                        </a>

                                        <x-button class="ml-4">
                                            {{ __('Registrar') }}
                                        </x-button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <div class="absolute w-full bottom-0 left-0">
            <div class="flex gap-2">
                <div class="flex-1 bg-green-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-blue-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-yellow-500 h-1 rounded-md"></div>
                <div class="flex-1 bg-red-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-gray-700 h-1 rounded-md"></div>
            </div>
        </div>
    </div>
</x-guest-layout>

<x-guest-layout>
    <div class="relative bg-zinc-100">
        <div class="absolute w-full top-0 left-0">
            <div class="flex gap-2">
                <div class="flex-1 bg-green-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-blue-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-yellow-500 h-1 rounded-md"></div>
                <div class="flex-1 bg-red-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-gray-700 h-1 rounded-md"></div>
            </div>
        </div>
        <section class="relative container mx-auto p-14 min-h-screen flex items-center justify-center">
            <div class="h-full">
                <!-- Left column container with background-->
                <div class="flex h-full flex-wrap items-center justify-center lg:justify-between">
                    <div class="shrink-1 mb-12 grow-0 basis-auto md:mb-0 md:w-9/12 md:shrink-0 lg:w-6/12 xl:w-6/12">
                        <img src="https://flowbite.s3.amazonaws.com/blocks/marketing-ui/authentication/illustration.svg"
                            class="w-full" alt="Sample image" />
                    </div>

                    <!-- Right column container -->
                    <div class="md:mb-0 md:w-8/12 lg:w-5/12 xl:w-6/12">
                        <div class="w-full bg-white rounded-xl border shadow-lg" style="z-index: 1;">
                            <div class="p-6 space-y-4">
                                <div
                                    class="text-xl flex items-center before:mt-0.5 before:flex-1 before:border-t before:border-indigo-300 after:mt-0.5 after:flex-1 after:border-t after:border-indigo-300">
                                    <div class="flex items-center justify-center mx-4">
                                        <a href="#">
                                            <div class="flex gap-4 items-center text-indigo-600">
                                                <i class="fa-solid fa-briefcase text-3xl"></i>
                                                {{-- <img src="{{ asset('assets/img/favicon/favicon.ico') }}" alt class="w-12 ml-[-10px]"> --}}
                                                <div class="flex flex-col">
                                                    <span class="text-base font-semibold"> Bienvenido a
                                                        la</span>
                                                    <span class="text-sm italic -mt-1"> BOLSA LABORAL </span>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <x-validation-errors />
                                <div class="text-sm font-normal text-center">
                                    Por favor, ingrese con su cuenta
                                </div>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf

                                    <div>
                                        <x-label for="email" value="{{ __('Email') }}" />
                                        <x-input id="email" class="block mt-1 w-full" type="email" name="email"
                                            :value="old('email')" required autofocus autocomplete="username" />
                                    </div>

                                    <div class="mt-4">
                                        <x-label for="password" value="{{ __('Password') }}" />
                                        <x-input id="password" class="block mt-1 w-full" type="password"
                                            name="password" required autocomplete="current-password" />
                                    </div>

                                    <div class="block mt-4">
                                        <label for="remember_me" class="flex items-center">
                                            <x-checkbox id="remember_me" name="remember" />
                                            <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                                        </label>
                                    </div>

                                    <div class="flex items-center justify-between my-4">
                                        @if (Route::has('password.request'))
                                            <a class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
                                                href="{{ route('password.request') }}">
                                                {{ __('Forgot your password?') }}
                                            </a>
                                        @endif

                                        <x-button class="ml-4">
                                            {{ __('Log in') }}
                                        </x-button>
                                    </div>
                                    <div class="text-center">
                                        <a href="{{ route('register') }}"
                                            class="text-xs sm:text-sm  font-base hover:underline text-indigo-500 hover:text-indigo-600">
                                            ¿Aún no tienes una cuenta? <strong> Registrate</strong>
                                        </a>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </section>
        <div class="absolute w-full bottom-0 left-0">
            <div class="flex gap-2">
                <div class="flex-1 bg-green-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-blue-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-yellow-500 h-1 rounded-md"></div>
                <div class="flex-1 bg-red-600 h-1 rounded-md"></div>
                <div class="flex-1 bg-gray-700 h-1 rounded-md"></div>
            </div>
        </div>
    </div>
</x-guest-layout>

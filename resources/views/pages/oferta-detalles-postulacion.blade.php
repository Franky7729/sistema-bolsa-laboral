@section('title', 'Ofertas - Más Detalles')
@section('header', 'Postulación')
@section('section', 'Postular')

<div class="h-screen flex">
    <!-- Lado de la imagen -->
    <div class="w-1/2 bg-cover bg-center"
        style="background-image: url('https://i0.wp.com/itusers.today/wp-content/uploads/2021/11/10-habilidades-que-debes-cultivar-para-postular-a-una-oferta-laboral.jpg?fit=1200%2C750&ssl=1');">
    </div>

    <!-- Lado del formulario -->
    <div class="w-1/2 flex justify-center items-center bg-white p-12">
        <div class="w-full max-w-md">
            @if (session()->has('message'))
                <div class="bg-green-100 border border-green-400 text-green-700 px-4 py-3 rounded relative"
                    role="alert">
                    {{ session('message') }}
                </div>
            @endif

            <form wire:submit.prevent="store">

                <div class="mb-4">
                    <label for="user_name" class="block text-gray-700 text-sm font-bold mb-2">Nombre de Usuario:</label>
                    <h1>{{ $usuarioLogueado->name }} {{ $usuarioLogueado->apellidos }} </h1>
                </div>
                <div class="mb-4">
                    <label for="oferta_laboral_id" class="block text-gray-700 text-sm font-bold mb-2">Oferta
                        Laboral:</label>
                    <select wire:model="postulacion.oferta_laboral_id"
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                        @foreach ($ofertasLaborales as $id => $titulo)
                            <option value="{{ $id }}" @if ($id == $ofertaLaboralSeleccionadaId) selected @endif>
                                {{ $titulo }}</option>
                        @endforeach
                    </select>
                    @error('postulacion.oferta_laboral_id')
                        <span class="text-red-500 text-xs italic">{{ $message }}</span>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="fecha_hora_postulacion" class="block text-gray-700 text-sm font-bold mb-2">Fecha y Hora
                        de Postulación:</label>
                    <input type="datetime-local" wire:model="postulacion.fecha_hora_postulacion"
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                    @error('postulacion.fecha_hora_postulacion')
                        <span class="text-red-500 text-xs italic">{{ $message }}</span>
                    @enderror
                </div>

                <div class="mb-4">
                    <label for="tipo" class="block text-gray-700 text-sm font-bold mb-2">Tipo:</label>
                    <input type="text" wire:model="postulacion.tipo"
                        class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline">
                    @error('postulacion.tipo')
                        <span class="text-red-500 text-xs italic">{{ $message }}</span>
                    @enderror
                </div>

                <div class="mb-6 ">
                    <label for="seleccionado"
                        class="block text-gray-700 text-sm font-bold mb-2 hidden">Seleccionado:</label>
                    <input type="checkbox" wire:model="postulacion.seleccionado" class="leading-tight hidden">
                    {{-- @error('postulacion.seleccionado') <span class="text-red-500 text-xs italic">{{ $message }}</span> @enderror --}}
                </div>

                <div class="w-full mt-4">
                    <x-label value="Documento PDF" class="font-bold" />
                    <div class="border border-gray-300 rounded-lg">
                        <label class="text-white text-sm rounded-t-lg bg-gray-600 focus:bg-gray-600 active:bg-gray-700 inline-flex items-center justify-center w-full px-4 py-2 cursor-pointer">
                            <i class="fa-solid fa-upload mr-1"></i>Cargar PDF
                            <input wire:model="document" type="file" hidden accept="application/pdf"/>
                        </label>

                        <div wire:loading wire:target="document" class="w-full">
                            <div id="alert-4" class="flex items-center justify-center p-3 mb-4 text-yellow-800 rounded-lg bg-yellow-50" role="alert">
                                <i class="fa-solid fa-circle-exclamation"></i>
                                <div class="ml-2 text-xs font-medium">
                                    Espere un momento por favor, el documento se está procesando.
                                </div>
                            </div>
                        </div>

                        <div class="p-2 py-2.5">
                            {{-- @dd($document) --}}
                            @if ($this->document)
                                <div class="text-center text-sm">
                                    Documento cargado: {{ $document->getClientOriginalName() }}
                                </div>
                            @else
                                <div class="h-full w-full text-center flex flex-col items-center justify-center">
                                    <span class="text-small text-gray-500">Ningún documento seleccionado</span>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>

                <button type="submit"
                    class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline">Enviar
                    Postulación</button>
            </form>
        </div>
    </div>
</div>

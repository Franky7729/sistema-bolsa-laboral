-- Creación de la base de datos
CREATE DATABASE IF NOT EXISTS bolsa_laboral;

-- Seleccionar la base de datos
USE bolsa_laboral;

-- Creación de la tabla 'empresas'
CREATE TABLE empresas (
    id INT AUTO_INCREMENT PRIMARY KEY,
    razon_social VARCHAR(255),
    ruc VARCHAR(15),
    direccion VARCHAR(255),
    telefono VARCHAR(15),
    correo VARCHAR(255),
    id_usuario INT,
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id)
);

-- Creación de la tabla 'usuarios'
CREATE TABLE usuarios (
    id INT AUTO_INCREMENT PRIMARY KEY,
    nombres VARCHAR(100),
    apellidos VARCHAR(100),
    dni VARCHAR(15),
    direccion VARCHAR(255),
    telefono VARCHAR(15),
    usuario VARCHAR(50),
    contraseña VARCHAR(50),
    id_rol INT,
    ruta_foto VARCHAR(255),
    ruta_cv VARCHAR(255),
    FOREIGN KEY (id_rol) REFERENCES roles(id)
);

-- Creación de la tabla 'oferta_laboral'
CREATE TABLE oferta_laboral (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_empresa INT,
    titulo VARCHAR(255),
    descripcion TEXT,
    fecha_publicacion DATE,
    fecha_cierre DATE,
    remuneracion DECIMAL(10, 2),
    ubicacion VARCHAR(255),
    tipo ENUM('presencial', 'remoto'),
    limite_postulantes INT
);

-- Creación de la tabla 'postulaciones'
CREATE TABLE postulaciones (
    id INT AUTO_INCREMENT PRIMARY KEY,
    id_usuario INT,
    id_oferta INT,
    fecha_hora_postulacion DATETIME,
    estado_actual ENUM('abierto', 'cerrado'),
    usuario_seleccionado INT,
    FOREIGN KEY (id_usuario) REFERENCES usuarios(id),
    FOREIGN KEY (id_oferta) REFERENCES oferta_laboral(id),
    FOREIGN KEY (usuario_seleccionado) REFERENCES usuarios(id)
);

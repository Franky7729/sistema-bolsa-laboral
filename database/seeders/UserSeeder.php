<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::create([
            'name' => 'Manuel CM',
            'nombres' => 'Manuel',
            'apellidos' => 'Chunca Mamani',
            'email' => 'manuel.chunca@upeu.edu.pe',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni' => '77298560',
            'direccion' => 'Av. Vilcabamba',
            'telefono' => '997448549',
        ]);
        User::create([
            'name' => 'Frank GCM',
            'nombres' => 'Manuel',
            'apellidos' => 'Chunca Mamani',
            'email' => 'frank.chunca@upeu.edu.pe',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni' => '77596200',
            'direccion' => 'Av. brasil',
            'telefono' => '9974458612',
        ]);
        User::create([
            'name' => 'Dan ECP',
            'nombres' => 'Dan Elioth',
            'apellidos' => 'Condori Pongo',
            'email' => 'dan.condori@upeu.edu.pe',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni' => '77295260',
            'direccion' => 'Av. Lima',
            'telefono' => '994512654',
        ]);
        User::create([
            'name' => 'Frank MM',
            'nombres' => 'Frank Josué',
            'apellidos' => 'Machaca Molleapaza',
            'email' => 'frank.machaca@upeu.edu.pe',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni' => '77485130',
            'direccion' => 'Av. Cusco',
            'telefono' => '997445120',
        ]);
        User::create([
            'name' => 'Frank DCH',
            'nombres' => 'Frank Diego',
            'apellidos' => 'Choquehuanca Huallpa',
            'email' => 'frank.choquehuanca@upeu.edu.pe',
            'email_verified_at' => now(),
            'password' => bcrypt('12345678'),
            'dni' => '77485130',
            'direccion' => 'Av. Cusco',
            'telefono' => '997445120',
        ]);

        $superAdminUser = User::where('email', 'manuel.chunca@upeu.edu.pe')->first();
        $adminUser1 = User::where('email', 'frank.chunca@upeu.edu.pe')->first();
        $adminUser2 = User::where('email', 'dan.condori@upeu.edu.pe')->first();
        $postulanteUser1 = User::where('email', 'frank.machaca@upeu.edu.pe')->first();
        $postulanteUser2 = User::where('email', 'frank.choquehuanca@upeu.edu.pe')->first();

        // Obtener los IDs de los roles
        $superAdminRoleId = Role::where('name', 'Super-Admin')->first()->id;
        $adminRoleId = Role::where('name', 'Administrador')->first()->id;
        $postulanteRoleId = Role::where('name', 'Postulante')->first()->id;

        // Asignar roles a los usuarios
        $superAdminUser->roles()->attach($superAdminRoleId);
        $adminUser1->roles()->attach($adminRoleId);
        $adminUser2->roles()->attach($adminRoleId);
        $postulanteUser1->roles()->attach($postulanteRoleId);
        $postulanteUser2->roles()->attach($postulanteRoleId);
    }
}

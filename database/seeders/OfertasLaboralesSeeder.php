<?php

namespace Database\Seeders;

use App\Models\Oferta_laboral;
use Illuminate\Database\Seeder;

class OfertasLaboralesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Oferta_laboral::create([
            'titulo' => 'Director de TI',
            'descripcion' => 'Se necesita urgente para esa área, con experiencia de 1 año o recién egresado',
            'fecha_publicacion' => '15 de abril',
            'fecha_cierre' => '20 de abril',
            'remuneracion' => '1500',
            'ubicacion' => 'Al centro de Juliaca',
            'tipo' => 1,
            'cantidad' => '10',
            'empresa_id' => 1,
        ]);
        Oferta_laboral::create([
            'titulo' => 'Desarrollador Web',
            'descripcion' => 'Se necesita urgente para esa área, con experiencia de 1 año o recién egresado',
            'fecha_publicacion' => '10 de abril',
            'fecha_cierre' => '20 de abril',
            'remuneracion' => '3500',
            'ubicacion' => 'Salida Arequipa',
            'tipo' => 2,
            'cantidad' => '15',
            'empresa_id' => 1,
        ]);
        Oferta_laboral::create([
            'titulo' => 'Gestor de Proyectos',
            'descripcion' => 'Se necesita urgente para esa área, con experiencia de 1 año o recién egresado',
            'fecha_publicacion' => '05 de abril',
            'fecha_cierre' => '30 de abril',
            'remuneracion' => '4500',
            'ubicacion' => 'Salida Cusco',
            'tipo' => 1,
            'cantidad' => '20',
            'empresa_id' => 2,
        ]);
        Oferta_laboral::create([
            'titulo' => 'Ingeniero de Software Senior',
            'descripcion' => 'Se busca ingeniero con al menos 5 años de experiencia en desarrollo de software.',
            'fecha_publicacion' => '10 de mayo',
            'fecha_cierre' => '25 de mayo',
            'remuneracion' => '6000',
            'ubicacion' => 'Lima',
            'tipo' => 2,
            'cantidad' => '5',
            'empresa_id' => 2,
        ]);

        Oferta_laboral::create([
            'titulo' => 'Analista de Datos',
            'descripcion' => 'Empresa de consultoría busca analista de datos con experiencia en SQL y Python.',
            'fecha_publicacion' => '02 de mayo',
            'fecha_cierre' => '20 de mayo',
            'remuneracion' => '4000',
            'ubicacion' => 'Lima',
            'tipo' => 1,
            'cantidad' => '8',
            'empresa_id' => 2,
        ]);

        Oferta_laboral::create([
            'titulo' => 'Especialista en Marketing Digital',
            'descripcion' => 'Se requiere especialista con conocimientos en SEO, SEM y redes sociales.',
            'fecha_publicacion' => '15 de mayo',
            'fecha_cierre' => '30 de mayo',
            'remuneracion' => '3800',
            'ubicacion' => 'Trujillo',
            'tipo' => 1,
            'cantidad' => '3',
            'empresa_id' => 2,
        ]);
    }
}

<?php

namespace Database\Seeders;

use App\Models\Empresa;
use App\Models\User;
use Illuminate\Database\Seeder;

class EmpresaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Empresa::create([
            'razon_social' => 'Universidad Peruana Unión',
            'ruc' => '202020202026',
            'direccion' => 'Av. Brasil',
            'telefono' => '99744848',
            'correo' => 'upeujuliaca@gmail.com',
            'creador' => User::where('email', 'manuel.chunca@upeu.edu.pe')->first()->name,
            'user_id' => User::where('email', 'frank.chunca@upeu.edu.pe')->first()->id,
        ]);

        Empresa::create([
            'razon_social' => 'Clinica Americana',
            'ruc' => '202326541200',
            'direccion' => 'Av. Brasil',
            'telefono' => '99744848',
            'correo' => 'clinicaamericana@gmail.com',
            'creador' => User::where('email', 'manuel.chunca@upeu.edu.pe')->first()->name,
            'user_id' => null,
        ]);

        Empresa::create([
            'razon_social' => 'Colegio Santo Domingo',
            'ruc' => '20321654984510',
            'direccion' => 'Av. Brasil',
            'telefono' => '99744848',
            'correo' => 'colegiosd@gmail.com',
            'creador' => User::where('email', 'manuel.chunca@upeu.edu.pe')->first()->name,
            'user_id' => null,
        ]);

        Empresa::create([
            'razon_social' => 'Mercado Hola',
            'ruc' => '201010101011',
            'direccion' => 'Jr. Vilcabamba',
            'telefono' => '987654321',
            'correo' => 'mercadohola@gmail.com',
            'creador' => User::where('email', 'manuel.chunca@upeu.edu.pe')->first()->name,
            'user_id' => null,
        ]);

        Empresa::create([
            'razon_social' => 'Coliseo Manco Capac',
            'ruc' => '202320201984',
            'direccion' => 'Av. Jaramilluyoc',
            'telefono' => '997448512',
            'correo' => 'coliseomc@gmail.com',
            'creador' => User::where('email', 'manuel.chunca@upeu.edu.pe')->first()->name,
            'user_id' => null,
        ]);
    }
}

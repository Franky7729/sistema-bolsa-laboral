<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('postulacions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('oferta_laboral_id');
            $table->foreign('oferta_laboral_id')->references('id')->on('oferta_laborals')->onDelete('cascade');
            $table->timestamp('fecha_hora_postulacion')->nullable();
            $table->enum('tipo', [1, 2])->default(2);
            $table->enum('seleccionado', [1, 2])->nullable();
            $table->string('ruta_pdf')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('postulacions');
    }
};
